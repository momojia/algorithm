package org.hegang.algorithm.bubblesort;


import org.hegang.algorithm.common.RandomNumber;

/**
 * @ClassName BubbleSortMain
 * @Describe: 冒泡排序
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 0:23 2019/6/28
 * @Modified By:
 * @Version V1.0
 */
public class Main {

    public static void main(String[] args) {
        RandomNumber randomNumber = new RandomNumber();
        //int[] bubbleSortArray = randomNumber.generateRandomNumberByRandom(0,100,20);
        int[] bubbleSortArray = randomNumber.generateRandomNumberByMath(0, 100, 20);
        int[] optimizationBubbleSortArray = randomNumber.generateRandomNumberByMath(0, 100, 20);

        BubbleSort bubbleSort = new BubbleSort();
        System.out.println("未优化冒泡排序");
        //Date startTime = new Date(System.currentTimeMillis());
        bubbleSort.bubbleSort(bubbleSortArray);
        //Date endTime = new Date(System.currentTimeMillis());
        System.out.println("优化的冒泡排序");
        bubbleSort.optimizationBubbleSort(optimizationBubbleSortArray);
    }
}
