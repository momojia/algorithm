package org.hegang.algorithm.sort;

import java.util.Arrays;

/**
 * @ClassName Test
 * @Describe: TODO
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 16:55 2020/5/14
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class Test {
    public static void main(String[] args) {
        Integer[] testSort = {1,2,3,2,6,3,8};
        Arrays.sort(testSort, new CustomizedComparator());
        Arrays.asList(testSort).forEach(item-> System.out.print(item+" "));
    }
}
