package org.hegang.algorithm.sort;

import java.util.Comparator;

/**
 * @ClassName ConstomComparator
 * @Describe: 返回值小于零，则不交换两个o1和o2的位置；如果返回值大于零，则交换o1和o2的位置
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 16:51 2020/5/14
 * @Modified_By: TODO
 * @Version: V1.0
 */

/**
 * 升序排序
 */
public class CustomizedComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1-o2;
    }
}
