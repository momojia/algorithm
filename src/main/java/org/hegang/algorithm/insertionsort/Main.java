package org.hegang.algorithm.insertionsort;

import org.hegang.algorithm.common.RandomNumber;

import java.util.Arrays;

/**
 * @ClassName InsertionSortMain
 * @Describe: 插入排序
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 14:57 2019/7/9
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class Main {
    public static void main(String[] args) {
        RandomNumber randomNumber = new RandomNumber();
        int[] insertionSort = randomNumber.generateRandomNumberByRandom(0, 100, 30);
        int[] insertionSortTwo = randomNumber.generateRandomNumberByMath(0, 100, 30);

        InsertionSort.insertionSort(insertionSort);
        System.out.println(Arrays.toString(insertionSort));

        InsertionSort.optimizationInsertionSort(insertionSortTwo);
        System.out.println(Arrays.toString(insertionSortTwo));
    }
}
