package org.hegang.algorithm.quicksort;

import org.hegang.algorithm.common.RandomNumber;

import java.util.Arrays;

/**
 * @ClassName QuickSortMain
 * @Describe: 快速排序
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 0:55 2019/6/29
 * @Modified By:
 * @Version V1.0
 */
public class Main {
    public static void main(String[] args) {
        RandomNumber randomNumber = new RandomNumber();
        int[] quickSortArray = randomNumber.generateRandomNumberByRandom(0, 100, 30);
        //排序前输出
        System.out.println("排序前输出");
        System.out.println(Arrays.toString(quickSortArray));

        QuickSort quickSort = new QuickSort();
        System.out.println("排序后输出");
        quickSort.quickSort(quickSortArray, 0, quickSortArray.length - 1);
        //排序后输出
        System.out.println(Arrays.toString(quickSortArray));

        //易于理解的快速排序
        int[] optimizationQuickSort = randomNumber.generateRandomNumberByRandom(0, 100, 30);
        quickSort.optimizationQuickSort(optimizationQuickSort, 0, optimizationQuickSort.length - 1);
        System.out.println("=======================================");
        System.out.println(Arrays.toString(optimizationQuickSort));
    }
}
