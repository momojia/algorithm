package org.hegang.algorithm.multhread;

import java.util.concurrent.*;

/**
 * @ClassName multipleThread
 * @Describe: Java jdk多线程的几种实现方式，Thread，Runnable，Callable，线程池,还有一种是spring搞的线程池
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 15:23 2020/5/2
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class MultipleThread {

    /**
     * 线程池
     * 跟数据库连接池类似
     * 避免了线程的创建和销毁造成的额外开销
     * <p>
     * java.util.concurrent
     * <p>
     * Executor    负责现成的使用和调度的根接口
     * |--ExecutorService    线程池的主要接口
     * |--ThreadPoolExecutor    线程池的实现类
     * |--ScheduledExecutorService    接口，负责线程的调度
     * |--ScheduledThreadPoolExecutor    (extends ThreadPoolExecutor implements ScheduledExecutorService)
     * <p>
     * <p>
     * Executors工具类
     * 提供了创建线程池的方法
     */

    public static void main(String[] args) {

        // 使用Executors创建线程池，不建议这样做，会造成oom出现错误
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<>();
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(10, 10, 60, TimeUnit.MINUTES, blockingQueue);

        // 为线程分配任务，使用submit方法
        // 传入的方法可以是Runnable的实现类，也可以是Callable的实现类
        // 实际开发中这里要处理业务

        ThreadPoolDemo poolDemo = new ThreadPoolDemo();
        for (int i = 0; i <= 5; i++) {
            executorService.submit(poolDemo);
        }
        executorService.shutdown();

    }

}
