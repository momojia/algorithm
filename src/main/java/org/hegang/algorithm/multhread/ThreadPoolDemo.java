package org.hegang.algorithm.multhread;

/**
 * @ClassName ThreadPoolDemo
 * @Describe: TODO
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 16:00 2020/5/2
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class ThreadPoolDemo implements Runnable {
    /**
     * 实际开发这里要处理业务,多线程是共享数据的
     */
    private int i = 0;

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + "---" + i++);

    }
}
