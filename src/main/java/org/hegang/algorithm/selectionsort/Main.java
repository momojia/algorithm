package org.hegang.algorithm.selectionsort;

import org.hegang.algorithm.common.RandomNumber;

import java.util.Arrays;

/**
 * @ClassName SelectionSortMain
 * @Describe: 选择排序
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 21:52 2019/7/3
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class Main {
    public static void main(String[] args) {
        RandomNumber randomNumber = new RandomNumber();
        int[] selectionSort = randomNumber.generateRandomNumberByRandom(0, 100, 30);
        SelectionSort.selectionSort(selectionSort);
        System.out.println(Arrays.toString(selectionSort));

    }
}
