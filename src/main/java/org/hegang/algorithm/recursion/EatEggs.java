package org.hegang.algorithm.recursion;

/**
 * @ClassName EatEggs
 * @Describe: 递归
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 16:17 2020/5/14
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class EatEggs {

    int totalCount;

    public int eatEggs(int n) {

        if (n == 1 || n == 2) {
            return totalCount = n;
        } else {
            totalCount = eatEggs(n - 1) + eatEggs(n - 2);
            return totalCount;
        }

    }
}
