package org.hegang.algorithm.common;

import java.util.Random;

/**
 * @ClassName RandomNumber
 * @Describe: 根据参数生成随机整数
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 1:51 2019/6/28
 * @Modified By:
 * @Version V1.0
 */
public class RandomNumber {

    /**
     * 通过Random类生成指定参数的随机整数[minValue,maxValue + minValue)
     *
     * @param minValue
     * @param maxValue
     * @param range    generate number that how much
     * @return
     */
    public int[] generateRandomNumberByRandom(int minValue, int maxValue, int range) {
        int[] randomArray = new int[range];
        //随机种子
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < range; i++) {
            randomArray[i] = random.nextInt(maxValue) + minValue;
        }
        return randomArray;
    }

    /**
     * 通过Math生成指定参数的随机整数[minValue,maxValue + minValue)
     *
     * @param minValue
     * @param maxValue
     * @param range
     * @return
     */
    public int[] generateRandomNumberByMath(int minValue, int maxValue, int range) {
        int[] randomArray = new int[range];
        for (int i = 0; i < range; i++) {
            randomArray[i] = (int) (Math.random() * maxValue + minValue);
        }
        return randomArray;
    }
}
