package org.hegang.algorithm.common;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * @ClassName DiffTimeUtil
 * @Describe: 通过Calendar简单计算时间差
 * @Author: gang.he
 * @Email: SmileSkylife@outlook.com
 * @Date: Created in 0:08 2019/7/2
 * @Modified_By: TODO
 * @Version: V1.0
 */
public class DiffTimeUtil {
    public static String diffTimeByCalendar(String originDate) throws ParseException {
        //简单计算，后期优化
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = simpleDateFormat.parse(originDate);

        //当前时间
        Calendar currentTime = Calendar.getInstance();
        Calendar originTime = Calendar.getInstance();
        originTime.setTime(date);

        int currentYearTime = currentTime.get(Calendar.YEAR);
        int originYearTime = originTime.get(Calendar.YEAR);
        int currentDateTime = currentTime.get(Calendar.DATE);
        int originDateTime = originTime.get(Calendar.DATE);
        int currentMonthTime = currentTime.get(Calendar.MONTH);
        int originMonthTime = originTime.get(Calendar.MONTH);

        return currentYearTime - originYearTime + "年" + (currentMonthTime - originMonthTime) + "月" + (currentDateTime - originDateTime);
    }

    /**
     * 通过Period简单计算时间差
     *
     * @param yeal
     * @param month
     * @param day
     * @return
     */
    public static String diffTimeByPeriod(int yeal, int month, int day) {
        LocalDate localDate = LocalDate.now();
        LocalDate oldDate = LocalDate.of(yeal, month, day);
        Period period = Period.between(localDate, oldDate);
        return period.getYears() + "年" + period.getMonths() + "月" + period.getDays();
    }

    /**
     * 通过Duration简单计算时间差
     *
     * @return
     */
    public static String diffTimeByDuration() {
        //当前的时间
        Instant instantOne = Instant.now();
        System.out.println("instantOne：" + instantOne);
        //当前时间+10秒后的时间
        Instant instantTwo = instantOne.plus(Duration.ofSeconds(10));
        System.out.println("instantTwo：" + instantTwo);
        //当前时间+125天后的时间
        Instant instantThree = instantOne.plus(Duration.ofDays(125));
        System.out.println("instantThree：" + instantThree);
        System.out.println("以毫秒计的时间差：" + Duration.between(instantOne, instantTwo).toMillis());
        System.out.println("以秒计的时间差：" + Duration.between(instantOne, instantThree).getSeconds());
        return null;
    }

    public static long diffTimeByChronoUnit() {
        LocalDate startDate = LocalDate.of(2003, Month.MAY, 9);
        System.out.println("开始时间：" + startDate);

        LocalDate endDate = LocalDate.of(2015, Month.JANUARY, 26);
        System.out.println("结束时间：" + endDate);

        long daysDiff = ChronoUnit.DAYS.between(startDate, endDate);
        System.out.println("两个时间之间的天数差为：" + daysDiff);
        return daysDiff;
    }

    /**
     * TODO 有时间优化封装
     *
     * @param originDate
     * @throws ParseException
     */
    public static void diffTimeBySimpleDateFormat(Date originDate) throws ParseException {
        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        /*天数差*/
        Date fromDate1 = simpleFormat.parse("2018-03-01 12:00");
        Date toDate1 = simpleFormat.parse("2018-03-12 12:00");
        long from1 = fromDate1.getTime();
        long to1 = toDate1.getTime();
        int days = (int) ((to1 - from1) / (1000 * 60 * 60 * 24));
        System.out.println("两个时间之间的天数差为：" + days);

        /*小时差*/
        Date fromDate2 = simpleFormat.parse("2018-03-01 12:00");
        Date toDate2 = simpleFormat.parse("2018-03-12 12:00");
        long from2 = fromDate2.getTime();
        long to2 = toDate2.getTime();
        int hours = (int) ((to2 - from2) / (1000 * 60 * 60));
        System.out.println("两个时间之间的小时差为：" + hours);

        /*分钟差*/
        Date fromDate3 = simpleFormat.parse("2018-03-01 12:00");
        Date toDate3 = simpleFormat.parse("2018-03-12 12:00");
        long from3 = fromDate3.getTime();
        long to3 = toDate3.getTime();
        int minutes = (int) ((to3 - from3) / (1000 * 60));
        System.out.println("两个时间之间的分钟差为：" + minutes);
    }

    public static void main(String[] args) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            date = simpleDateFormat.parse("2004/03/26");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String diffTime = null;
        try {
            diffTime = diffTimeByCalendar(simpleDateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            System.out.println(diffTime);
        }
    }
}
